/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import React from 'react';
import {
  StyleSheet,
  StatusBar,
} from 'react-native';
import Navigation from './src/navigation/Navigation'
import * as Layout from './Layout'
const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor="transparent" translucent={true}/>
      <Navigation/>
    </>
  );
};

const styles = StyleSheet.create({
  
});

export default App;
