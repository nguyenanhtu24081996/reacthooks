import React, { useState } from 'react'
import {
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    Image,
    StyleSheet,
    StatusBar,
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
const { width, height } = Dimensions.get('window')

export default function List(props) {
    // constructor(props){
    //     super(props)
    //     this.state={
    //         itemLike:this.props.isLiked
    //     }
    // }

    // componentWillReceiveProps(nextProps){
    //     this.setState({
    //         itemLike:nextProps.isLiked
    //     })
    // }

    // shouldComponentUpdate(nextProps, nextState){
    //         return this.state.itemLike!==nextState.isLiked
        // this.setState({
        //     itemLike:nextProps.isLiked
        // })
    // }
    const avatar = props.avatar;
    const name = props.name;
    const address = props.address
        return (
            <View>
                <View style={styles.container}>
                    <View style={styles.viewImage}>
                        <TouchableOpacity >
                            <Image style={styles.image} source={{ uri: avatar }}></Image>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.viewText}>
                        <View style={styles.nameText}>
                            <Text>{name}</Text>
                        </View>
                        <View style={styles.addressText}>
                            <Text style={{ fontStyle: 'italic' }}>{address}</Text>
                        </View>
                    </View>
                    <View style={styles.likeArea}>
                        <TouchableOpacity >
                            <Icon name='thumbs-up' size={40} color={'black'}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
}
const styles = StyleSheet.create({
    container: {
        width: width,
        height: height / 12.8,
        flexDirection: "row",
        borderBottomColor: '#ECECEC',
        borderBottomWidth: 1,
        paddingVertical:StatusBar.currentHeight
    },
    viewImage: {
        width: height / 13,
        height: height / 13,
        backgroundColor: '#FAFAFA',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: height / 16,
        height: height / 16,
        borderRadius: height / 28
    },
    viewText: {
        flex: 1,
        height: height / 13,
        backgroundColor: '#FAFAFA',
    },
    nameText: {
        flex: 1.5,
        borderBottomColor: '#ECECEC',
        borderBottomWidth: 1,
        justifyContent: 'center',
    },
    addressText: {
        flex: 1,
        justifyContent: 'center',
    },
    likeArea: {
        width: height / 13,
        height: height / 13,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FAFAFA',
    },

})