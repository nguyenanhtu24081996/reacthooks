import React, {useState} from 'react'
import{
    View,
    Text
} from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {HOME, SETTINGS} from '../navigation/NavigationName'
import HomeScreen from '../screen/HomeScreen'
import SettingsScreen from '../screen/SettingsScreen'
export default function MyTab(){
    const Tab = createBottomTabNavigator();
    return(
        <Tab.Navigator>
            <Tab.Screen name={HOME} component={HomeScreen} />
            <Tab.Screen name={SETTINGS}component={SettingsScreen} />
        </Tab.Navigator>
    )
}
