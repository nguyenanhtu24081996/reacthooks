import React, { useState, useEffect } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    ActivityIndicator
} from 'react-native'
import MainScreen from '../screen/MainScreen'
import NavigationTabBottom from '../navigation/NavigationTabBottom'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {BOTTOM_TAB_BAR, LOGIN} from "./NavigationName";

export default function Navigation() {
    const Stack = createStackNavigator()
    return (
        <NavigationContainer>
            <Stack.Navigator headerMode={'none'}>
                <Stack.Screen name={LOGIN} component={MainScreen} />
                <Stack.Screen name={BOTTOM_TAB_BAR} component={NavigationTabBottom} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}