import React, {useState} from 'react'
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    ActivityIndicator
} from 'react-native'

export default function Hello(){
    return(
        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
            <Text>Settings!</Text>
        </View>
    )
}