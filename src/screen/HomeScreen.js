import React, { useState, useEffect } from 'react'
import {
    Text,
    View,
    FlatList,
    RefreshControl
} from 'react-native'
import axios from 'axios'
const url = 'http://5de1cab90929540014dc335f.mockapi.io/list_hr'
import List from '../component/List'
import { useCallback } from 'react'
import { set } from 'react-native-reanimated'
export default function ListScreen() {
    const [data, setData] = useState([]);
    const [refreshing, setRefreshing] = useState(false);
    async function getData() {
        return await axios.get(url);
    }

    useEffect(() => {
        getData().then((response) => {
            setData(response.data);
            setRefreshing(false)
            console.log('data = ', response.data);
        }).catch((error) => {
            console.error('fetch error = ', error);
        });
    }, []);
    
    const onRefresh = useCallback(() => {
        setRefreshing(true);
        setTimeout(function(){
            if(data.length < 10){
                getData().then((res) => {
                    setData(res.data)
                    setRefreshing(false)
                }).catch((error) =>{
                    console.error('fetch error = ', error);
                });
            }
        }, 2000)
        
    } , () => {useEffect()})

    return (
        <FlatList
            data={data}
            renderItem={({ item }) =>
                {
                    return(
                        <List
                        avatar={item.avatar}
                        name={item.name}
                        address={item.address}
                        />
                    )
                }
            }
            keyExtractor={item => item.id}
            refreshControl={
                <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}/>
            }
        />
    )
}