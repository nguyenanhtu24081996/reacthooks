import React, { useState, useEffect } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Dimensions
} from 'react-native'
const { width, height } = Dimensions.get('window');
import Loading from '../component/Loading'
import {BOTTOM_TAB_BAR} from "../navigation/NavigationName";
export default function MainScreen(props) {
    const [count, setCount] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
     
    function _nativeTo(){
        const {navigation} = props;
        setIsLoading(true)
        setTimeout(function() {
            navigation.navigate(BOTTOM_TAB_BAR)
        }, 3000)
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={styles.button}
                onPress={() => _nativeTo()}
            >
                <Text>LoginScreen</Text>
            </TouchableOpacity>
            <Loading
                isLoading={isLoading}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        width,
        height,
    },
    button:{
        width:'90%',
        height:height/15,
        justifyContent:"center",
        alignItems:'center',
        backgroundColor:'yellow',
        borderRadius:10
    }
}) 